#define H(bit) (_BV(bit) | _BV(bit+4))
#define L(bit) _BV(bit)

typedef struct {
  uint8_t led;  // The high-low mask bits for the charlieplex
  int16_t mv;   // The actual voltage in mv of the Vref
} LedPoint;

/* The led byte contains two nybles:

 Bit 0-4 the bits that need to be driven, this is what ends up in the DDR register.
 Bit 5-7 the bit that need to be set, this is what ends up in the PORT register.
*/

// The LED bit mask and value and the center voltage in mv value for each LED
#define LEDS 10

const LedPoint LP[LEDS] = {
  {H(1)|L(0),  400}, // > 4V
  {L(1)|H(0),  370}, // 3.7V
  {H(2)|L(1),  250}, // 2.5V
  {L(2)|H(1),  165}, // 1.65V   
  {H(3)|L(2),  140}, // 1.4V
  {L(3)|H(2),  120}, // 1.2V
  {H(0)|L(3),   90}, // 0.9V
  {L(0)|H(3),    0}, // 0V  
  {H(1)|L(3), -130}, // -1.3V
  {L(1)|H(3), -200}  // < -2V
};

uint16_t voltageToLeds(int16_t mv) {
  int16_t diff = 10000;
  int8_t best = -1;
  
  for (uint8_t i=0;i<LEDS;i++) {
    int16_t d = mv-LP[i].mv;
    if (abs(d) < abs(diff)) {
      best = i;
      diff = d;
    }
  }
  
  if (best < 0) {
    return 0xff;    
  }
  
  uint16_t leds = 1<<best;
 
  if (abs(diff) >= 10) {
    if (best < LEDS-1 && diff < 0) {
      leds |= _BV(best+1);
    }
    if (best > 1 && diff > 0) {
      leds |= _BV(best-1);
    }
  }
  
  return leds;
}
